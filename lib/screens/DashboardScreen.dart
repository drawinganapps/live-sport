import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sport_app/controller/filter_controller.dart';
import 'package:sport_app/data/dummy.dart';
import 'package:sport_app/helpers/color_helper.dart';
import 'package:sport_app/widgets/category_widget.dart';
import 'package:sport_app/widgets/live_match_card_widget.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final categories = DummyData.categories;
    final liveMatches = DummyData.liveMatches;
    return GetBuilder<FilterController>(builder: (controller) {
      return Scaffold(
        body: ListView(
          padding: EdgeInsets.zero,
          physics: const BouncingScrollPhysics(),
          children: [
            Container(
              height: screenHeight * 0.42,
              padding: const EdgeInsets.only(top: 30),
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.center,
                      colors: [
                    ColorHelper.secondaryColor,
                    ColorHelper.mainColor
                  ])),
              child: Stack(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        color: ColorHelper.secondaryColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50),
                        )),
                    height: screenHeight * 0.2,
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.menu),
                            iconSize: 35,
                            color: Colors.grey),
                        Container(
                          margin: const EdgeInsets.only(top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(right: 5),
                                child: Image.asset('assets/icons/o.png',
                                    width: 15, height: 15),
                              ),
                              Text('SKY SPORT',
                                  style: GoogleFonts.rubik(
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.5,
                                      height: 1.5))
                            ],
                          ),
                        ),
                        IconButton(
                            onPressed: () {},
                            icon: const Icon(Icons.search),
                            iconSize: 35,
                            color: Colors.grey),
                      ],
                    ),
                  ),
                  Positioned(
                      top: 100,
                      child: Container(
                        height: 250,
                        width: screenWidth,
                        padding: const EdgeInsets.only(
                            left: 25, right: 25, top: 10, bottom: 10),
                        child: Stack(
                          children: [
                            Container(
                              height: 190,
                              padding:
                                  const EdgeInsets.only(left: 15, right: 15),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  gradient: const LinearGradient(colors: [
                                    ColorHelper.purple,
                                    ColorHelper.purple,
                                    ColorHelper.blue,
                                    ColorHelper.blue
                                  ]),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color:
                                            ColorHelper.purple.withOpacity(0.4),
                                        blurRadius: 5,
                                        spreadRadius: 2,
                                        offset: const Offset(0.0, 0.75))
                                  ]),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.2),
                                            shape: BoxShape.circle),
                                        child: Image.asset(
                                            'assets/icons/barcelona.png',
                                            width: 35,
                                            height: 35),
                                      ),
                                      Text('FC Barcelona',
                                          style: GoogleFonts.rubik(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12))
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.2),
                                            shape: BoxShape.circle),
                                        child: Image.asset(
                                            'assets/icons/madrid.png',
                                            width: 35,
                                            height: 35),
                                      ),
                                      Text('Real Madrid',
                                          style: GoogleFonts.rubik(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                                top: 175,
                                right: screenWidth * 0.25,
                                child: Container(
                                  width: 150,
                                  decoration: BoxDecoration(
                                      color: ColorHelper.secondaryColor,
                                      borderRadius: BorderRadius.circular(15)),
                                  padding: const EdgeInsets.all(5),
                                  child: Row(
                                    children: [
                                      Image.asset('assets/icons/la-liga.png',
                                          height: 20, width: 20),
                                      Container(
                                        margin: EdgeInsets.only(left: 5),
                                      ),
                                      Text('Today, 08.30 PM',
                                          style: GoogleFonts.rubik(
                                              fontWeight: FontWeight.w400,
                                              color: Colors.grey))
                                    ],
                                  ),
                                )),
                          ],
                        ),
                      )),
                  Positioned(
                      right: screenWidth * 0.30,
                      top: 85,
                      child: Image.asset('assets/img/joung.png',
                          width: 140, height: 200, fit: BoxFit.cover)),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Text('Sports',
                  style: GoogleFonts.rubik(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w500)),
            ),
            SizedBox(
              height: 100,
              child: ListView(
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                children: List.generate(categories.length, (index) {
                  return GestureDetector(
                    child: CategoryWidget(
                        category: categories[index],
                        isSelected: controller.selectedFilter == index),
                    onTap: () {
                      controller.changeFilter(index);
                    },
                  );
                }),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 10, top: 10),
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Live Matches',
                      style: GoogleFonts.rubik(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w500)),
                  Row(
                    children: [
                      Text('Show all',
                          style: GoogleFonts.rubik(
                              color: Colors.grey.withOpacity(0.8),
                              fontSize: 15)),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.grey.withOpacity(0.5),
                      )
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 250,
              child: ListView(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: List.generate(liveMatches.length, (index) {
                return LiveMatchCardWidget(live: liveMatches[index]);
              })),
            )
          ],
        ),
        bottomNavigationBar: Container(
          height: 90,
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
              ),
              color: ColorHelper.secondaryColor),
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.space_dashboard),
                    iconSize: 35,
                  ),
                  Container(
                    height: 5,
                    width: 5,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: ColorHelper.purple,
                    ),
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.rss_feed),
                    iconSize: 35,
                    color: Colors.grey.withOpacity(0.5),
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.notifications),
                    iconSize: 35,
                    color: Colors.grey.withOpacity(0.5),
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.person),
                    iconSize: 35,
                    color: Colors.grey.withOpacity(0.5),
                  )
                ],
              ),
            ],
          ),
        ),
      );
    });
  }
}
