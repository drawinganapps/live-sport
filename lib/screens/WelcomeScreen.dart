import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sport_app/helpers/color_helper.dart';
import 'package:sport_app/routes/AppRoutes.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          children: [
            Container(
              height: screenWidth * 1.2,
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    ColorHelper.secondaryColor,
                    ColorHelper.secondaryColor,
                    ColorHelper.mainColor
                  ])),
              child: Stack(
                children: [
                  Container(
                      width: screenWidth,
                      padding: const EdgeInsets.only(top: 50),
                      decoration: const BoxDecoration(
                          color: ColorHelper.secondaryColor,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(50),
                            bottomRight: Radius.circular(50),
                          ),
                          image: DecorationImage(
                              image: AssetImage('assets/img/league1.jpg'),
                              fit: BoxFit.cover,
                              opacity: 0.2)),
                      clipBehavior: Clip.antiAlias,
                      child: Image.asset('assets/img/messi.png', fit: BoxFit.cover,
                          height: screenHeight * 0.4)),
                  Positioned(
                      top: screenHeight * 0.420,
                      right: screenWidth * 0.35,
                      child: Column(
                        children: [
                          Container(
                            height: 55,
                            width: 55,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: Colors.white, width: 1)),
                            clipBehavior: Clip.antiAlias,
                            padding: const EdgeInsets.all(1),
                            child: Container(
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  gradient: LinearGradient(colors: [
                                    ColorHelper.purple,
                                    ColorHelper.purpleDark,
                                  ])),
                              child: Image.asset('assets/img/volleyball.png',
                                  width: 50, height: 50),
                            ),
                          ),
                          Text('SKY SPORT',
                              style: GoogleFonts.rubik(
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.5,
                                  height: 1.5))
                        ],
                      ))
                ],
              ),
            ),
            SizedBox(
              width: screenWidth * 0.83,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 40),
                    child: Text('All Brights Channel',
                        style: GoogleFonts.rubik(
                            fontWeight: FontWeight.w500, fontSize: 35)),
                  ),
                  Text('Live Sports!',
                      style: GoogleFonts.rubik(
                          fontWeight: FontWeight.w700,
                          fontSize: 40,
                          foreground: Paint()
                            ..shader = const LinearGradient(
                              colors: <Color>[
                                ColorHelper.purple,
                                ColorHelper.purpleDark
                              ],
                            ).createShader(
                                const Rect.fromLTWH(0.0, 0.0, 200.0, 70.0)))),
                  Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 20),
                    child: Text(
                        'O Sport is a Application that You Can Watch All Sports Games Live & Online',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.rubik(
                            fontWeight: FontWeight.w400,
                            fontSize: 15,
                            letterSpacing: 0.78,
                            color: Colors.grey)),
                  ),
                ],
              ),
            ),
          ],
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 20),
          height: screenHeight * 0.2,
          padding: const EdgeInsets.only(left: 15, right: 15, top: 20),
          child: ListView(
            physics: const BouncingScrollPhysics(),
            padding: EdgeInsets.zero,
            children: [
              ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                      primary: ColorHelper.purple,
                      shape: const StadiumBorder() // Background color
                      ),
                  child: SizedBox(
                    width: screenWidth * 0.8,
                    height: 60,
                    child: Center(
                      child: Text('Register',
                          style: GoogleFonts.rubik(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 18)),
                    ),
                  )),
              Container(
                margin: const EdgeInsets.only(top: 7, bottom: 7),
              ),
              OutlinedButton(
                onPressed: () {
                  Get.toNamed(AppRoutes.DASHBOARD);
                },
                style: OutlinedButton.styleFrom(
                    shape: const StadiumBorder() // Background color
                    ),
                child: SizedBox(
                    width: screenWidth * 0.8,
                    height: 60,
                    child: Center(
                      child: Text(
                        'Login',
                        style: GoogleFonts.rubik(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 18),
                      ),
                    )),
              )
            ],
          ),
        )
      ],
    ));
  }
}
