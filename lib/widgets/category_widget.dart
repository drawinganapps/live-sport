import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sport_app/helpers/color_helper.dart';
import 'package:sport_app/models/sport_category_model.dart';

class CategoryWidget extends StatelessWidget {
  final SportCategoryModel category;
  final bool isSelected;
  const CategoryWidget({Key? key, required this.category, required this.isSelected}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 15, right: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 5),
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isSelected ? ColorHelper.purpleDark : ColorHelper.mainColor,
                border: Border.all(width: 1, color:ColorHelper.secondaryColor)
            ),
            child: Image.asset('assets/icons/${category.icon}', width: 35, height: 35),
          ),
          Text(isSelected ? category.name : '', textAlign: TextAlign.center, style: GoogleFonts.rubik(
              color: Colors.white,
              fontWeight: FontWeight.w300
          )),
        ],
      ),
    );
  }
  
}