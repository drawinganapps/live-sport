import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sport_app/helpers/color_helper.dart';
import 'package:sport_app/models/live_matches_model.dart';

class LiveMatchCardWidget extends StatelessWidget {
  final LiveMatchesModel live;
  const LiveMatchCardWidget({Key? key, required this.live}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: 180,
      padding: const EdgeInsets.only(left: 15),
      child: Stack(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 25),
            child:  Container(
              height: 200,
              padding: const EdgeInsets.only(left: 15, right: 15, top: 55),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(width: 1, color: ColorHelper.purple),
                color: ColorHelper.secondaryColor
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.asset('assets/icons/${live.homeIcon}', width: 35, height: 35),
                      Image.asset('assets/icons/${live.awayIcon}', width: 35, height: 35),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 25),
                    child:  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(live.homeName, style: GoogleFonts.rubik()),
                        Container(
                          margin: const EdgeInsets.only(left: 15),
                        ),
                        Text(live.homeScore.toString(), style: GoogleFonts.rubik(
                          color: Colors.grey
                        )),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child:  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(live.awayName, style: GoogleFonts.rubik()),
                        Container(
                          margin: const EdgeInsets.only(left: 15),
                        ),
                        Text(live.awayScore.toString(), style: GoogleFonts.rubik(
                            color: Colors.grey
                        )),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ),
          Positioned(
              left: 55,
              child: Container(
                decoration: BoxDecoration(
                  color: ColorHelper.mainColor,
                  border: Border.all(width: 1, color: ColorHelper.secondaryColor),
                  shape: BoxShape.circle
                ),
            padding: const EdgeInsets.all(10),
            child: Image.asset('assets/icons/${live.leagueIcon}', width: 30, height: 30),
          ))
        ],
      ),
    );
  }

}