class LiveMatchesModel {
  final String homeName;
  final String homeIcon;
  final int homeScore;
  final String awayName;
  final String awayIcon;
  final int awayScore;
  final String leagueName;
  final String leagueIcon;

  LiveMatchesModel(this.homeName, this.homeIcon, this.homeScore, this.awayName, this.awayIcon, this.awayScore, this.leagueName, this.leagueIcon);
}