class SportCategoryModel {
  final String name;
  final String icon;

  SportCategoryModel(this.name, this.icon);
}