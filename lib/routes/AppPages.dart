import 'package:get/get.dart';
import 'package:sport_app/controller/filter_binding.dart';
import 'package:sport_app/screens/DashboardScreen.dart';
import 'package:sport_app/screens/WelcomeScreen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.MAIN,
        page: () => const WelcomeScreen(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const DashboardScreen(),
        binding: FilterBinding(),
        transition: Transition.rightToLeft
    ),
  ];
}
