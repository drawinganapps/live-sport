import 'package:flutter/material.dart';

class ColorHelper {
  static const Color mainColor = Color.fromRGBO(32,37,49, 1);
  static const Color secondaryColor = Color.fromRGBO(42,47,59, 1);
  static const Color purple = Color.fromRGBO(146, 99, 251, 1);
  static const Color purpleDark = Color.fromRGBO(123, 81, 239, 1);
  static const Color blue = Color.fromRGBO(40,64,179, 1);
}