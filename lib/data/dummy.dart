import 'package:sport_app/models/live_matches_model.dart';
import 'package:sport_app/models/sport_category_model.dart';

class DummyData {
  static  List<SportCategoryModel> categories = [
    SportCategoryModel('Football', 'football.png'),
    SportCategoryModel('Basket', 'basketball.png'),
    SportCategoryModel('Rugby', 'rugby.png'),
    SportCategoryModel('Cricket', 'cricket.png'),
    SportCategoryModel('Badminton', 'shuttlecock.png'),
  ];

  static List<LiveMatchesModel> liveMatches = [
    LiveMatchesModel('FC Barcelona', 'barcelona.png', 2, 'Real Madrid', 'madrid.png', 1, 'La Liga', 'la-liga.png'),
    LiveMatchesModel('PSG', 'psg.png', 3, 'Lyon', 'olympique.png', 1, 'Ligue 1', 'league 1.png'),
    LiveMatchesModel('Chelsea', 'chelsea.png', 1, 'Arsenal', 'arsenal.png', 1, 'Premier League', 'epl.png'),
  ];
}