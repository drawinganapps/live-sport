import 'package:flutter/material.dart';
import 'package:sport_app/helpers/color_helper.dart';

ThemeData darkTheme = ThemeData(
  brightness: Brightness.dark,
  scaffoldBackgroundColor: ColorHelper.mainColor,
  highlightColor: Colors.transparent,
  splashColor: Colors.transparent,
);
